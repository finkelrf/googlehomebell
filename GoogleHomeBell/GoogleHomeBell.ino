#include <ESP8266WiFi.h>
#include <esp8266-google-home-notifier.h>
#include <MQTT.h>
#include "config.h"

#define Sprintln(a) (Serial.println(a))
#define Sprint(a) (Serial.print(a))
// #define Sprintln(a) 
// #define Sprint(a)

WiFiClient net;
MQTTClient mqtt;
GoogleHomeNotifier google_home;
int ADC_THRESHOLD = 650;

struct flags {
  bool ringing;
  bool ringing_mqtt;
  bool ringing_google_home;
  bool ping_rcv;
  bool search_google_home;
};

struct flags flags = {false, false, false, false, true};

void messageReceived(String &topic, String &payload) {
  Sprintln("incoming: " + topic + " - " + payload);
  if (payload == "ring"){
    Sprintln("Ring detected");
    flags.ringing = true;
  }
  else if (payload == "ping"){
    flags.ping_rcv = true;
  }
}

bool try_connect_wifi(){
  if (WiFi.status() != WL_CONNECTED) {
    Sprint(".");
    return false;
  }
  return true;
}

bool try_connect_mqtt(){
  if (!mqtt.connect(mqtt_client, mqtt_user, mqtt_pass)) {
    Sprint("-");
    return false;
  }else{
    mqtt.subscribe(root_topic+"/command");  
  }
  return true;
}

void print_wifi_info(){
  Sprintln("");
  Sprintln("connected.");
  Sprint("IP address: ");
  Sprintln(WiFi.localIP());
}

void check_adc(){
  if (analogRead(A0) > ADC_THRESHOLD){
    flags.ringing = true;
  }
}

void setup(){  
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  
  mqtt.begin(mqtt_server, mqtt_port, net);
  mqtt.onMessage(messageReceived);
  
  Sprintln(ssid);
}

void check_flags(){
  if(flags.ringing){
    flags.ringing_mqtt = true;
    flags.ringing_google_home = true;
    flags.ringing = false;
  }
  else if (flags.ringing_mqtt){
    if(mqtt.publish(root_topic,"ringing")){
      flags.ringing_mqtt = false;
    }
  }
  else if(flags.ringing_google_home){
    if(!google_home.play(ring_url)){
      Sprintln(google_home.getLastError());
    }else{
      flags.ringing_google_home = false;
    }
  }
  else if (flags.ping_rcv){
    if(mqtt.publish(root_topic,"pong")){
      flags.ping_rcv = false;
    }
  }
  else if (flags.search_google_home){
    if (WiFi.isConnected()){
      if (!google_home.device(displayName, "en")){
        Sprintln(google_home.getLastError());
      }
      else{
        flags.search_google_home = false;
        Sprint("found Google Home(");
        Sprint(google_home.getIPAddress());
        Sprint(":");
        Sprint(google_home.getPort());
        Sprintln(")");
        mqtt.publish(root_topic, "Google home found");
      }
    }
  }
}

void loop(){
  if (!WiFi.isConnected()){
    try_connect_wifi();
  }
  if (WiFi.isConnected() && !mqtt.connected()){
    try_connect_mqtt();
  }
  check_adc();
  mqtt.loop();
  check_flags();
  delay(10);
}