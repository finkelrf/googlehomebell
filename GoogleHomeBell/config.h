// WiFi variables
const char* ssid     = "MyNiceSSID";
const char* password = "MyNicePassword";

const char* mqtt_client = "ESP8266"
const char* mqtt_server = "my.nyce.mqtt.broker.com";
int mqtt_port = 1883;
const char* mqtt_user = "NiceMQTTUser";
const char* mqtt_pass = "VeryNiceAndSecurePass";
String root_topic = "/bell";

//Name of my google home
const char* displayName = "google home";

//phone ring
const char* ring_url = "https://drive.google.com/uc?export=download&id=1i53ktCCr6hXI3waRJVijN69FQZBwFrNh";
