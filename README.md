# Google Home Bell
This project uses a esp8266 to sense a bell/intercom signal and use your Google Home as a bell to let you know there is people at your door.

I recently moved to a apartment without the internal unit of the intercom, but its wires were exposed. By checking the voltage between the wires I saw that when someone pressed the intercom the voltage jump from 12v to 24v. Using a voltage divider I can sense the voltage on the A0 of a esp8266 board and then play a bell song on my Google Home.

## Software

This project uses [esp8266-google-home-notifier](https://github.com/horihiro/esp8266-google-home-notifier) and [MQTT](https://github.com/256dpi/arduino-mqtt) libraries.
